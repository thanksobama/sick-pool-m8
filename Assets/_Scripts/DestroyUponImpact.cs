﻿using UnityEngine;
using System.Collections;

public class DestroyUponImpact : MonoBehaviour {

	void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Cue")
        {
            Destroy(gameObject);
        }
    }
}
