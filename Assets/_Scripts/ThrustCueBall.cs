﻿using UnityEngine;
using System.Collections;

public class ThrustCueBall : MonoBehaviour
{
    private float thrustForce ;
    public float maxThrustForce;
    public float thrustIncrement;
    public float thrustDecrement;
    private Rigidbody rb;
    private float incrementatingForce;
    public GameController gameController;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {

        if (Input.GetButton("Fire1"))
        {
            if (thrustForce + thrustIncrement <= maxThrustForce)
            {
                thrustForce += thrustIncrement;
                gameController.SetForce(thrustForce);
                Debug.Log(thrustForce);
            }
            else
            {
                thrustForce = maxThrustForce;
                gameController.SetForce(thrustForce);
            }
        }
        else
        {
            if (thrustForce - thrustDecrement >= 0.0f)
            {
                thrustForce -= thrustDecrement;
                gameController.SetForce(thrustForce);
                Debug.Log(thrustForce);
            }
            else
            {
                thrustForce = 0.0f;
                gameController.SetForce(thrustForce);
            }
        }
        
        if (Input.GetButton("Submit"))
        {
            rb.AddForce(0, 0, -thrustForce, ForceMode.Impulse);
            thrustForce = 0;
        }
    }
}

