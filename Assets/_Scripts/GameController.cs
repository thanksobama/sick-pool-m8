﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameController : MonoBehaviour {

    public Slider forceSlider;
    public Text forceText;
    private float force;

    // Use this for initialization
    void Start () {
        forceText.text = "0";
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void SetForce(float newForceValue)
    {
        force = newForceValue;
        UpdateForce();
    }

    void UpdateForce()
    {
        forceSlider.value = force;
        forceText.text = Mathf.Round(force).ToString();
    }
}
